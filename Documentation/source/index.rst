Getting started: ECH2O User Guide
=================================

This document provides a tutorial that guides users through the process of preparing the files, running and visualizing an ech2o simulation. 


Installation
============

Download the installation package and follow the instructions for your operating system. The installation package includes the sample dataset used in this tutorial. Alternativelly, you can download the sample files for the `ECH2O case study <http://hs.umt.edu/RegionalHydrologyLab/software/docs-ech2o/CaseStudy-ECH2O.zip>`_
 
To pre-process inputs and to visualize model results you will need to instal PCRASTER, which can be downloaded for free from http://pcraster.geo.uu.nl 


Tutorial
=========

.. toctree::
   :maxdepth: 3

   Setup
   CaseStudy
   Keywords




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

