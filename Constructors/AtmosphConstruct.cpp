/*******************************************************************************
 * Ech2o, a spatially-distributed, ecohydrologic simulator
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the 
 *     University of Montana.
 *
 *     Ech2o is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Ech2o is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Ech2o.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta
 *******************************************************************************/
/*
 * AtmosphConstruct.cpp
 *
 *  Created on: Oct 14, 2009
 *      Author: Marco Maneta
 */

#include <errno.h>
#include <cstring>
#include "Atmosphere.h"

Atmosphere::Atmosphere(Control &ctrl){

	try{

		//Read the zones map and writes the dimensions of the grid
		_zones = new grid(ctrl.path_ClimMapsFolder + ctrl.fn_climzones,
				ctrl.MapType);
		_NRows = _zones->r;
		_NCols = _zones->c;
		_dx = _zones->dx;
		CountNumZones(); //Reads the _zones grid and fills variable _nzones with the number of zones. This is the number of zones in the zone map
		_NZns = 0; //set to zero the initial zones in the climate time series files. This is the number of zones in the climate time series


			_vSsortedGridTotalCellNumber = 0;
        	#pragma omp parallel for
			for (unsigned int i = 0; i < _nzones; i++) {
				_vSortedGrid[i].cells = SortGrid(_vSortedGrid[i].zone).cells; // fills the vectCells with actual domain cells (no nodata)
#pragma omp atomic
				_vSsortedGridTotalCellNumber += _vSortedGrid[i].cells.size();
		     }


		/*constructs the object using the basemap so
		 attributes (lat, long, nodata...) are copied*/
		_Ldown = new grid(*_zones);
		_Sdown = new grid(*_zones);
		_Tp = new grid(*_zones);
		_MaxTp = new grid(*_zones);
		_MinTp = new grid(*_zones);
		_Precip = new grid(*_zones);
		_Rel_humid = new grid(*_zones);
		_Wind_speed = new grid(*_zones);
		_Antr_heat = NULL; //initialized below protected by antropogenic_heat switch

		*_Ldown = *_zones;
		*_Sdown = *_zones;
		*_Tp = *_zones;
		*_MaxTp = *_zones;
		*_MinTp = *_zones;
		*_Precip = *_zones;
		*_Rel_humid = *_zones;
		*_Wind_speed = *_zones;

		_isohyet = new grid(ctrl.path_ClimMapsFolder + ctrl.fn_isohyet,
				ctrl.MapType);
		_rain_snow_temp = ctrl.snow_rain_temp;

		/*open climate data files*/

		try {
			ifLdown.open((ctrl.path_ClimMapsFolder + ctrl.fn_Ldown).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_Ldown; //echo_filenotfound_exception(ctrl.fn_Ldown.c_str(), "Dang! File not found: ");
			ifSdown.open((ctrl.path_ClimMapsFolder + ctrl.fn_Sdown).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_Sdown; //echo_filenotfound_exception(ctrl.fn_Sdown.c_str(), "Dang! File not found: ");
			ifTp.open((ctrl.path_ClimMapsFolder + ctrl.fn_temp).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_temp;
			ifMaxTp.open((ctrl.path_ClimMapsFolder + ctrl.fn_maxTemp).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_maxTemp;
			ifMinTp.open((ctrl.path_ClimMapsFolder + ctrl.fn_minTemp).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_minTemp;
			ifPrecip.open((ctrl.path_ClimMapsFolder + ctrl.fn_precip).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_precip;
			ifRelHumid.open(
					(ctrl.path_ClimMapsFolder + ctrl.fn_rel_humid).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_rel_humid;
			ifWindSpeed.open(
					(ctrl.path_ClimMapsFolder + ctrl.fn_wind_speed).c_str(),
					ios::binary);
			if (errno != 0)
				throw ctrl.fn_wind_speed;

			if(ctrl.sw_antr_heat){
				_Antr_heat = new grid(*_zones);
				*_Antr_heat = *_zones;

				ifAntrHeat.open((ctrl.path_ClimMapsFolder + ctrl.fn_antrop_heat).c_str(),
						ios::binary);
				if (errno != 0)
					throw ctrl.fn_antrop_heat;
			}

		} catch (string e) {
			cout << "Dang!!: cannot open " << e << "  file with error: " << strerror(errno) << endl;
			throw;
		}

#ifdef _OPENMP
UINT4 number_threads;
number_threads = omp_get_num_threads();
#endif
		size_t errCount = 0;
#pragma omp parallel num_threads(9) default(none) shared(cout, errCount, ctrl) if (number_threads > 1)
		{
			try {
#pragma omp sections
				{
					//Initiate Climate map returns the number of data written
#pragma omp section
					{
						if (InitiateClimateMap(ifLdown, *_Ldown)
								!= _vSsortedGridTotalCellNumber)
							throw string("incoming longwave");
					}

#pragma omp section
					{
						if (InitiateClimateMap(ifSdown, *_Sdown)
								!= _vSsortedGridTotalCellNumber)
							throw string("short wave");
					}
#pragma omp section
					{
						if (InitiateClimateMap(ifTp, *_Tp)
								!= _vSsortedGridTotalCellNumber)
							throw string("average air temperature");
					}
#pragma omp section
					{
						if (InitiateClimateMap(ifMaxTp, *_MaxTp)
								!= _vSsortedGridTotalCellNumber)
							throw string("maximum air temperature");
					}
#pragma omp section
					{
						if (InitiateClimateMap(ifMinTp, *_MinTp)
								!= _vSsortedGridTotalCellNumber)
							throw string("minimum air temperature");
					}
#pragma omp section
					{
						if (InitiateClimateMap(ifPrecip, *_Precip)
								!= _vSsortedGridTotalCellNumber)
							throw string("precipitation");
						AdjustPrecip(); // adjust precipitation with the isohyet map
					}
#pragma omp section
					{
						if (InitiateClimateMap(ifRelHumid, *_Rel_humid)
								!= _vSsortedGridTotalCellNumber)
							throw string("relative humidity");
					}
#pragma omp section
					{
						if (InitiateClimateMap(ifWindSpeed, *_Wind_speed)
								!= _vSsortedGridTotalCellNumber)
							throw string("windspeed");
					}
#pragma omp section
					{
					if(ctrl.sw_antr_heat)
						if (InitiateClimateMap(ifAntrHeat, *_Antr_heat)
								!= _vSsortedGridTotalCellNumber)
							throw string("anthropogenic heat");
					}
				} //sections
			} catch (string &e) {
				#pragma omp critical
				{
				cout << "Error: some sections of the domain was not filled with "
				<< e << " data." << endl << "Please verify that all the climate zones"
						" in the map are presented in the binary climate data file " << endl << "and that the n climate"
								" zones present are the first n zones in the binary climate data file " << endl;


				++errCount;
				}
			} //catch
		}//parallel region

		if (errCount != 0)
			throw;

	} catch (...) {
		cerr << "Cleaning the atmosphere..." << "\n";

		//clean up the mess...
		if (_zones)
			delete _zones;
		if (_zoneId)
			delete[] _zoneId;

		delete _Ldown;
		delete _Sdown;
		delete _Tp;
		delete _MaxTp;
		delete _MinTp;
		delete _Precip;
		delete _Rel_humid;
		delete _Wind_speed;
		delete _Antr_heat;

		if (_isohyet)
			delete _isohyet;

		throw;
	}
}
